import os
import subprocess


def realsplit(nf):
    return os.path.split(os.path.realpath(nf))


def Gitpull(nf_base):
    chemin, _ = realsplit(nf_base)
    os.chdir(chemin)
    subprocess.call(["git", "pull"])


def Gitpush(nf_base):
    chemin, fichier = realsplit(nf_base)
    os.chdir(chemin)
    subprocess.call(["git", "add", fichier])
    subprocess.call(["git", "commit", "-m", "'Commit automatique des notes'"])
    subprocess.call(["git", "push"])
