import math
from collections import defaultdict


def calculer_rangs(donnees):
    """donnees est un tableau de nombres ; renvoie le rang de chaque entrée dans le tableau"""
    donnees_positions = list(enumerate(donnees))
    donnees_positions.sort(key=lambda x: x[1] if x[1] is not None else 0., reverse=True)
    rang_courant = 1
    k = 0
    donnee_courante = None
    rangs = [None] * len(donnees)
    while k < len(donnees):
        if donnees_positions[k][1] != donnee_courante:
            rang_courant = k + 1
            donnee_courante = donnees_positions[k][1]
        if donnees_positions[k][1] is not None:
            rangs[donnees_positions[k][0]] = rang_courant
        k += 1
    return rangs


def enrichir_rangs(donnees, valref, critere, nom):
    """donnees est une liste de dictionnaires dont valref est une clé. Si critere est None : enrichit chaque ligne de
    donnees avec une clé nom de valeur le rang de valref dans la liste. Si critère est une chaine : les données sont
    séparées par valeurs de la clé critere """
    if critere is None:
        rangs = calculer_rangs([donnees[k][valref] for k in range(len(donnees))])
        for k in range(len(donnees)):
            donnees[k][nom] = rangs[k]
    else:
        donnees_crit = defaultdict(list)  # Pour empêcher le "if k not in d : d[k] = []"
        for k in range(len(donnees)):
            donnees_crit[donnees[k][critere]].append(donnees[k][valref])
        donnees_crit = dict(donnees_crit)
        rangs_crit = {c: calculer_rangs(donnees_crit[c]) for c in donnees_crit.keys()}
        for k in range(len(donnees)):
            donnees[k][nom] = rangs_crit[donnees[k][critere]].pop(0)


def histogramme(donnees, valref, critere, fcase=None, ncases=21):
    if critere is None:
        les_histo = [0] * ncases
        histo = lambda x: les_histo
    else:
        les_histo = {}

        def histo(x):
            try:  # "it's Easier to Ask Forgiveness than Permission"
                return les_histo[x[critere]]
            except KeyError:
                return [0] * ncases
    
    if fcase is None:
        def fcase(n):
            if n < 20.0:
                return int(math.floor(n))
            else:
                return 20
    
    for x in donnees:
        try:
            h = histo(x)
            c = fcase(x[valref])
            h[c] += 1
        except TypeError:
            continue
    
    return les_histo
