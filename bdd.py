import math
import sqlite3
import unicodedata


def sans_accent(s):
    """Normalise une chaine"""
    return unicodedata.normalize("NFKD", s).encode("ASCII", "ignore").decode("utf-8")


def ordonner_nom(s1, s2):
    """Relation d'ordre pour la BDD"""
    t1 = sans_accent(s1)
    t2 = sans_accent(s2)
    if t1 < t2:
        return -1
    elif t1 == t2:
        return 0
    else:
        return 1


def ordonner_question(t1, t2):
    """Relation d'ordre pour la BDD"""
    if t1[0] == "q" and t2[0] != "q":
        return -1
    if t1[0] != "q" and t2[0] == "q":
        return 1
    if t1[0] != "q" and t2[0] != "q":
        if len(t1) < len(t2):
            return -1
        elif len(t1) > len(t2):
            return +1
        elif t1 < t2:
            return -1
        elif t1 > t2:
            return +1
        else:
            return 0
    q1 = [int(x) if x.isdigit() else x for x in t1[1:].split(".")]
    q2 = [int(x) if x.isdigit() else x for x in t2[1:].split(".")]
    if q1 < q2:
        return -1
    if q1 > q2:
        return 1
    return 0


def prefixe_nom(s1, s2):
    """Relation d'ordre utilisée seulement pour sélectionner un nom d'élève"""
    t1 = sans_accent(s1.strip()).lower().replace(" ", "")
    t2 = sans_accent(s2.strip()).lower().replace(" ", "")
    if t2 == "":
        return 1
    if t1.startswith(t2):
        return 0
    else:
        return 1


def str_round(x, ndigits):
    if x is None:
        return None
    else:
        return ("{0:." + str(ndigits) + "f}").format(x)


class StdevFunc:
    """Fonction d'agrégation pour l'écart-type"""

    def __init__(self):
        self.M = 0.0
        self.S = 0.0
        self.k = 0

    def step(self, value):
        if value is None:
            return
        tM = self.M
        self.k += 1
        self.M += (value - tM) / self.k
        self.S += (value - tM) * (value - self.M)

    def finalize(self):
        if self.k < 1:
            return None
        return math.sqrt(self.S / (self.k))


class BDD_SQLite(sqlite3.Connection):
    IntegrityError = sqlite3.IntegrityError
    
    # @property
    # def IntegrityError(self) :
    #     return _IntegrityError

    def __init__(self, nom_fichier):
        """Ouvre une bdd sqlite et met en place les relations d'ordre et agrégations ci-dessus définies"""
        super().__init__(nom_fichier)
        self.row_factory = sqlite3.Row
        self.create_collation("PREFIXE_NOMS", prefixe_nom)
        self.create_collation("ORDONNER_NOMS", ordonner_nom)
        self.create_collation("ORDONNER_QUEST", ordonner_question)
        self.create_aggregate("STDEV", 1, StdevFunc)
        self.create_function("STRROUND", 2, str_round)
    
    @staticmethod
    def getKeys(cur):
        """Récupère les clés (champs) d'un curseur bdd, même s'il n'y a aucun enregistrement"""
        return [x[0] for x in cur.description]
