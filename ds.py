#!/usr/bin/env python3

import argparse
import os
import re
import sys

from bdd import BDD_SQLite
from git import Gitpull, Gitpush
from interf_batch import InterfaceBatch
from interf_clav import InterfaceClavier
from interf_csv import InterfaceCSV
from moteur import CreerDS, Initialiser
from pronote import Pronote, Bulletin, ECTS
from rapports import Rapport, Fiches


# Interface


def nf_preambule(nf_bdd):
    return os.path.join(os.path.dirname(os.path.realpath(nf_bdd)), "preambule.tex")


reg = re.compile('^[A-Za-z0-9*_]+$')


def valide(s):
    return reg.match(s)


def sanitize(args):
    if not valide(args.tbl_etu) or (hasattr(args, "tbl_ds") and not valide(args.tbl_ds)):
        raise ValueError("Mauvais nom de table")
    if args.tbl_etu == "classes_ds":
        raise ValueError("Nom interdit pour la table des étudiants")
    if hasattr(args, "doublon"):
        args.doublon = int(args.doublon)
        if args.doublon not in range(0, 4):
            raise ValueError("Action par défaut en cas de doublon invalide")
    if os.path.isfile(args.base) and not os.access(args.base, os.W_OK):
        raise ValueError("BDD non inscriptible")
    if hasattr(args, "classes"):
        for x in args.classes:
            if not valide(x):
                raise ValueError('Mauvais nom de classe')
    if hasattr(args, "options") and args.options is not None:
        for x in args.options:
            if not valide(x):
                raise ValueError("Mauvais nom d'option")
    if hasattr(args, "preambule"):
        if args.preambule is None:
            args.preambule = nf_preambule(args.base)
        else:
            if not os.path.isfile(args.preambule):
                raise ValueError("Le préambule doit être un fichier existant")


def traiter(args):
    sanitize(args)

    if args.commande == "gitpull":
        Gitpull(args.base)
    elif args.commande == "gitpush":
        Gitpush(args.base)
    else:
        bdd = BDD_SQLite(args.base)
        if args.commande == "corriger":
            if hasattr(args, "batch") and args.batch is not None:
                InterfaceBatch(bdd, args.tbl_etu, args.tbl_ds, args.batch,
                               args.doublon if hasattr(args, "doublon") else 0).Corriger()
            elif hasattr(args, "csv") and args.csv is not None:
                InterfaceCSV(bdd, args.tbl_etu, args.tbl_ds, args.csv,
                             args.doublon if hasattr(args, "doublon") else 0).Corriger()
            else:
                InterfaceClavier(bdd, args.tbl_etu, args.tbl_ds).Corriger()
        elif args.commande == "fiches":
            Fiches(args.tbl_ds, args.tbl_etu, bdd, args.preambule, args.classe, args.groupe, args.stat_groupe)
        elif args.commande == "pronote":
            Pronote(args.tbl_ds, args.classe, bdd)
        elif args.commande == "bulletin":
            Bulletin(bdd, args.periode, args.classe, args.option)
        elif args.commande == "ects":
            ECTS(bdd, args.periode, args.classe, args.option)
        elif args.commande == "rapport":
            Rapport(args.tbl_ds, args.tbl_etu, bdd, args.preambule)
        elif args.commande == "creer":
            CreerDS(bdd, args.tbl_ds, args.tbl_etu, args.classes, args.options, args.periode)
        elif args.commande == "init":
            Initialiser(bdd, args.tbl_etu)
        else:
            ValueError("ne devrait pas se produire")


def main():
    defaultDir = os.path.dirname(sys.argv[0])

    parser = argparse.ArgumentParser()
    # options communes
    fonda = argparse.ArgumentParser(add_help=False)
    fonda.add_argument("--base", help="Fichier de la BDD", default=defaultDir + "/base.sqlite")
    fonda.add_argument("--tbl-etu", help="Nom de la table des étudiants", default="etudiants", dest="tbl_etu")
    common = argparse.ArgumentParser(add_help=False, parents=[fonda])
    common.add_argument("tbl_ds", help="Nom de la table de DS")
    
    # gestion des commandes
    subparsers = parser.add_subparsers(dest="commande", title="Opérations", description="Opérations possibles")
    # corriger
    parser_corriger = subparsers.add_parser("corriger", parents=[common], help="Lance une session de correction")
    parser_corriger.add_argument("--batch", help="Traiter les notes depuis un fichier")
    parser_corriger.add_argument("--doublon", help="Action à effectuer en cas de doublon", default=0)
    parser_corriger.add_argument("--csv", help="Importer un tableau de notes en CSV")
    
    # fiches
    parser_fiches = subparsers.add_parser("fiches", parents=[common], help="Génère les fiches étudiants")
    parser_fiches.add_argument("--classe", help="Classe pour laquelle générer les fiches")
    parser_fiches.add_argument("--groupe", help="Groupe pour lequel générer les fiches")
    parser_fiches.add_argument("--stat-groupe", help="Afficher les statistiques par groupe sur les fiches",
                               action="store_true")
    parser_fiches.add_argument("--preambule", help="Fichier tex à inclure dans le préambule")
    #  pronote
    parser_pronote = subparsers.add_parser("pronote", parents=[common], help="Envoie les notes vers Pronote")
    parser_pronote.add_argument("--classe", help="Classe concernée", required=True)
    # bulletin
    parser_bulletin = subparsers.add_parser("bulletin", parents=[fonda], help="Envoie les commentaires vers Pronote")
    parser_bulletin.add_argument("--classe", help="Classe concernée", required=True)
    parser_bulletin.add_argument('--periode', help="Semestre S1 ou S2", required=True)
    parser_bulletin.add_argument('--option', help="Option concernée")

    # ects
    parser_ects = subparsers.add_parser("ects", parents=[fonda], help="Envoie les lettres ECTS")
    parser_ects.add_argument("--classe", help="Classe concernée", required=True)
    parser_ects.add_argument('--periode', help="Semestre S1 ou S2", required=True)
    parser_ects.add_argument('--option', help="Option concernée")
    # rapport
    parser_rapport = subparsers.add_parser("rapport", parents=[common], help="Génère le rapport général")
    parser_rapport.add_argument("--preambule", help="Fichier tex à inclure dans le préambule")
    # creer
    parser_creer = subparsers.add_parser("creer", parents=[common], help="Crée les tables pour un DS")
    parser_creer.add_argument('periode', help="Semestre S1 ou S2")
    parser_creer.add_argument('classes', nargs="+", help="Classes concernées")
    parser_creer.add_argument('--options', nargs="+", help="Options concernées")
    # git
    parser_gitpull = subparsers.add_parser("gitpull", parents=[fonda],
                                           help="Fait un git pull sur le dépôt qui contient la base")
    parser_gitpush = subparsers.add_parser("gitpush", parents=[fonda],
                                           help="Commite le fichier de la base puis git push sur la branche courante")

    # initialisation de la table etudiants
    parser_init = subparsers.add_parser("init", parents=[fonda], help="Initialise la base, crée la table des étudiants")
    
    args = parser.parse_args()
    if not hasattr(args, "commande") or args.commande is None:
        parser.print_help()
    else:
        traiter(args)


if __name__ == "__main__":
    main()
