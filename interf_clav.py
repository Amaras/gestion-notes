import os
import subprocess
import tempfile

from interf_texte import *
from moteur import PlusieursEleves, AucunEleve, MauvaisNbItems, ItemTropGrand


def editerTexte(ancienTexte):
    f = tempfile.NamedTemporaryFile(mode='w+t', delete=False)
    n = f.name
    f.write(ancienTexte)
    f.close()
    subprocess.call(["nano", "--softwrap", n])
    with open(n) as f:
        texte = f.read().strip()
    os.remove(n)
    return texte


class InterfaceClavier(InterfaceTexte):
    def __init__(self, fich_base, table_etu, table_ds_bn):
        super().__init__(fich_base, table_etu, table_ds_bn)
    
    def Corriger(self):
        try:
            while True:
                self.TraiterCopie()
        except Sortie:
            self.NbCopies()
            self.ds.commit()
            self.ds.close()
    
    def DesambiguerEleve(self, l_eleves):
        """Permet de choisir un élève parmi une liste."""
        reponse = -1
        print("Plusieurs choix possibles")
        for k in range(len(l_eleves)):
            print(k + 1, " : ", list(l_eleves[k]))
        while reponse not in range(len(l_eleves) + 1):
            try:
                reponse = int(input("Quel élève (0 pour revenir) ? "))
            except:
                reponse = -1
        if reponse == 0:
            return None
        return l_eleves[reponse - 1]
    
    def InputEleve(self):
        """Demande le nom d'un élève en vue de sa sélection."""
        eleve = None
        while eleve is None:
            nom = input("Entrez le nom d'un élève (---- pour sortir) ")
            if nom == "----":
                raise Sortie
            if " " in nom:
                (nom, prenom) = nom.split(" ")
            else:
                prenom = None
            try:
                eleve = self.ds.SelectionnerEleve(nom, prenom)
            except AucunEleve:
                print("Aucun résultat")
            except PlusieursEleves as e:
                eleve = self.DesambiguerEleve(e.liste)
        return eleve
    
    def InputNote(self, eleve, q_nb):
        """Réalise l'interface pour la fonction ParseNote. On ne fait que de l'affichage et des vérifications formelles."""
        les_items = self.ds.ItemsQuestion(q_nb)
        prompt = self.ds.NomQuestion(q_nb) + " ("
        if len(les_items) == 1:
            prompt += "un item sur " + str(les_items[0][1])
        else:
            prompt += "/".join([item[0] + " sur " + str(item[1]) for item in les_items])
        prompt += ") "
        ancien = self.ds.AncienneNote(eleve, q_nb)
        if ancien is not None:
            prompt += "(ancien : " + ancien + ") "
        return self.ParseNote(input(prompt))
    
    def InsereNote(self, id_eleve, q_nb, note, replace):
        """Effectue l'inscription d'une note dans la BDD. En cas de conflit (note déjà existante), demande confirmation si replace est False.
        Renvoie le fait qu'on a réussi l'inscription."""
        try:
            succes, repl = self.ds.EnregistrerNote(id_eleve, q_nb, note, replace)
            if repl:
                print("Remplacement de la note en question", self.ds.NomQuestion(q_nb))
            return succes
        except NoteExistante:
            reponse = ""
            while reponse not in ("o", "n"):
                reponse = input("Erreur d'intégrité, voulez-vous remplacer la note existante (o/n) ? ")
            if reponse == "o":
                return self.InsereNote(id_eleve, q_nb, note, True)
            else:
                return False
    
    def TraiterQuestion(self, eleve, q_nb):
        """Gère le processus de saisie d'une note pour une question, c'est-à-dire qu'on signale les saisies incorrectes détectées par les fonctions auxiliaires, qu'on vérifie que la question demandée existen qu'on a rentré le bon nombre d'items, que les notes sont possibles et tente l'inscription effective de cette note.
        Renvoie le fait qu'on a réussi ce qu'on voulait, ce qui permet à la fonction appelante de savoir si elle doit passer à la question suivante ou rester sur celle-ci."""
        try:
            (replace, repq, note) = self.InputNote(eleve, q_nb)
        except ValueError:
            print("Erreur de saisie")
            return q_nb, False
        except SaisieCommentaire as e:
            self.ds.ParseCommentaire(eleve["id"], e.texte)
            return q_nb, False
        if repq is None:
            question = self.ds.NomQuestion(q_nb)
        else:
            try:
                q_nb = self.ds.QuestionNommee(repq, q_nb)
                question = repq
            except ValueError:
                print("Question " + repq + " introuvable")
                return q_nb, False
        if note is None:
            return q_nb, repq is None
        if note == ActionNote.Delete:
            try:
                self.ds.EffacerNote(eleve["id"], question)
                return q_nb, True
            except:
                print("Impossible d'effacer la note pour la question", question)
                return q_nb, False
        try:
            return q_nb, self.InsereNote(eleve["id"], q_nb, note, replace)
        except MauvaisNbItems:
            print("Mauvais nombre d'items pour la question " + question)
            return q_nb, False
        except ItemTropGrand:
            print("Valeur trop élevée pour un item en question " + question)
            return q_nb, False
    
    def TraiterCopie(self):
        """Traite une copie en demandant d'abord à qui elle est, puis en itérant dans les questions du sujet"""
        try:
            eleve = self.InputEleve()
        except ValueError:
            print("Erreur de saisie")
            return
        print("Copie de ", list(eleve))
        q_nb = 0
        try:
            while q_nb < self.ds.NombreQuestions():
                (q_traitee, reussi) = self.TraiterQuestion(eleve, q_nb)
                if reussi:
                    q_nb = q_traitee + 1
                else:
                    q_nb = q_traitee
            print("Copie terminée")
        except FinCopie:
            pass
        self.NoteTotale(eleve)
        self.InputCommentaire(eleve["id"])
    
    def InputCommentaire(self, eleve_id):
        reponse = ""
        anciencom = self.ds.AncienCommentaire(eleve_id)
        if anciencom is None:
            anciencom = ""
        while reponse not in ("o", "n"):
            print("Commentaire actuel :", anciencom)
            reponse = input("Éditer commentaire (o/n) ? ")
        if reponse == "o":
            self.ds.EnregistrerCommentaire(eleve_id, editerTexte(anciencom))
