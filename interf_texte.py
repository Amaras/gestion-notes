from contextlib import suppress

from moteur import DS, NoteExistante


class Sortie(Exception):
    pass


import enum


class ActionNote(enum.Enum):
    Delete = 1


class FinCopie(Exception):
    pass


class SaisieCommentaire(Exception):
    def __init__(self, texte):
        self.texte = texte


class AbandonCopie(Exception):
    pass


class AbandonDebut(AbandonCopie):
    pass


class AbandonMilieu(AbandonCopie):
    def __init__(self, id_eleve):
        self.id_eleve = id_eleve


class ActionDoublon(enum.Enum):
    Demander = 0
    Ignorer = 1
    Ecraser = 2
    Completer = 3


def intornone(ch):
    ch = ch.strip()
    if ch == "" or ch.lower() == "none":
        return None
    else:
        return int(ch)


class InterfaceTexte:
    def __init__(self, fich_base, table_etu, table_ds_bn):
        self.ds = DS(fich_base, table_etu, table_ds_bn)
    
    def NoteTotale(self, eleve):
        """Totalise les points de la copie et affiche la note"""
        try:
            print("Note :", self.ds.NoteTotale(eleve["id"]))
        except:
            print("Erreur inattendue lors de la récupération du total")
    
    def NbCopies(self):
        with suppress(Exception):
            print("Vous avez corrigé", self.ds.select_from_result("COUNT(DISTINCT id_eleve)", "").fetchone()[0],
                  "copies")
    
    def ParseNote(self, chaine):
        """Traite une saisie visant à attribuer des points à une question.
        La chaine vide ou réduite à - passent à la question suivante sans rien faire.
        La chaine --- interrompt la correction de la copie.
        Si la chaine est de la forme qX:, fait passer la correction à la question qX.
        Si la chaine est de la forme qX:n, fait passer la correction à la question qX et tente directement d'attribuer la note n à cette question.
        Si la chaine est de la forme n, tente d'attribuer la note n à la question courante.
        Si la question comporte plusieurs items, on doit saisir tous les items en séparant par des /. Éventuellement, si des items sont non répondus, on met // pour signifier qu'on saute cet item.
        Précéder la note de ! permet d'éviter la confirmation d'écrasement.
        La fonction renvoie le fait qu'on ait demandé le remplacement d'une note existante, la question pour laquelle on souhaite inscrire une note et la note à inscrire."""
        if chaine == "" or chaine == "-":
            return False, None, None
        if chaine == "---":
            raise FinCopie
        if chaine[0] == "*":
            raise SaisieCommentaire(chaine[1:])
        if ":" in chaine:
            (question, ch_note) = chaine.split(":")
        else:
            (question, ch_note) = (None, chaine)
        if question is not None:
            question = question.strip()
            if question == "":
                raise ValueError
        if ch_note != "" and ch_note[0] == "!":
            replace = True
            ch_note = ch_note[1:]
        else:
            replace = False
        ch_note = ch_note.strip()
        if ch_note == "":
            return replace, question, None
        elif ch_note == "-":
            return replace, question, ActionNote.Delete
        else:
            return replace, question, [intornone(ch) for ch in ch_note.split('/')]


class InterfaceAuto(InterfaceTexte):
    def __init__(self, fich_base, table_etu, table_ds_bn, nom_fichier, action_doublon):
        super().__init__(fich_base, table_etu, table_ds_bn)
        self.ds.commit_note = False
        self.action_doublon = ActionDoublon(action_doublon)
        self.f = open(nom_fichier)
    
    def QueFaireDoublon(self):
        if self.action_doublon is not ActionDoublon.Demander:
            print("Copie déjà présente ; action prise :", self.action_doublon.name)
            return self.action_doublon
        reponse = ""
        while reponse.lower() not in ("i", "e", "c"):
            reponse = input(
                "Copie déjà présente ; ignorer, écraser, completer ? (i/e/c)\n(majuscule pour le faire par défaut à "
                "l'avenir) ")
        action = ActionDoublon(("i", "e", "c").index(reponse.lower()) + 1)
        if reponse.isupper():
            self.action_doublon = action
        return action
    
    def InsereNote(self, id_eleve, q_nb, note, replace):
        """Effectue l'inscription d'une note dans la BDD. En cas de conflit (note déjà existante), demande confirmation si replace est False.
        Renvoie le fait qu'on a réussi l'inscription."""
        try:
            succes, repl = self.ds.EnregistrerNote(id_eleve, q_nb, note, replace)
            if repl:
                print("Remplacement de la note en question", self.ds.QuestionNommee(q_nb))
            return succes
        except NoteExistante:
            print("Erreur d'intégrité en question", self.ds.QuestionNommee(q_nb))
            print("Abandon de la copie")
            raise AbandonMilieu(id_eleve)
    
    def Corriger(self):
        try:
            while True:
                try:
                    self.TraiterCopie()  # Il semblerait que cette fonction n'existe pas (au moins dans cette branche)
                except AbandonDebut:
                    print("Abandon de la copie dès le début")
                except AbandonMilieu as e:
                    print("Abandon au milieu de la copie de l'élève", e.id_eleve)
                    reponse = ""
                    while reponse not in ("o", "n"):
                        reponse = input("Voulez-vous effacer toutes ses notes (o/n) ? ")
                    if reponse == "o":
                        self.ds.EffacerToutesNotes(e.id_eleve)
                        self.ds.EffacerCommentaire(e.id_eleve)
        except Sortie:
            self.NbCopies()
            self.ds.commit()
            self.ds.close()
            self.f.close()
