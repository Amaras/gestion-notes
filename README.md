# Présentation

Ce script python vise à gérer des corrections de DS : calcul des notes sur 20, génération de fiches de résultats. Cela permet de ne plus écrire de notes sur les copies (ni globale, ni par question) mais seulement des annotations et remarques.

De par sa structure, il requiert Python 3.6+

# Utilisation de base

Le script python s'utilise depuis un terminal.
## Initialisation

Les données sont dans une base sqlite qui est idéalement créée (avec les tables idoines) par ```ds init```.

Par défaut, le script utilise une base nommée ```base.sqlite``` située dans le répertoire du script et considère que la table des étudiants s'appelle ```etudiants```. Toutes les commandes du script permettent de spécifier une autre base et un autre nom pour cette table.

Il y a aussi une table ```classes_ds``` qui permet d'associer à chaque DS des classes (éventuellement une option) et une période (voir plus bas).

## Création d'un DS
La commande
```
ds creer machin periode classe1 classe2 [--option opt]
```

crée les tables nécessaires à un ds de nom machin,
soit ```machin_struct```, ```machin_copie``` et ```machin_result```. La période (S1 ou S2, par exemple) sert pour le calcul éventuel des moyennes et des commentaires du bulletin. La liste des classes permet de déterminer quels sont les élèves concernés par ce DS, idem concernant l'éventuelle option.

```machin_struct``` contient la structure du ds : pour chaque item, le nombre de
points de barème et le poids dans la note sur 20 (le script fera une règle de
trois).
Cette table doit être remplie à la main, à l'aide d'un éditeur de table par
exemple (sqliteman, etc.).

```machin_result``` sera remplie par le script au fur et à mesure de la correction.

## Correction
On démarre une session de correction interactive pour le DS machin avec ```ds corriger machin```.

Si on a un ordinateur mais pas d'accès au script, on peut enregistrer sa saisie dans un fichier texte simple. Les différences avec la syntaxe de la session interactive sont :
  * la fin de chaque copie doit être signalée par ```---```, même si la dernière question a été atteinte
  * le commentaire de la copie est donné en commençant une unique ligne par ```*```

On importe ces saisies par ```ds corriger machin --batch nom_fichier```.

## Génération de rapports et fiches de notes

Ces fonctions requièrent une installation LaTeX (latexmk et xelatex).

```
ds rapport machin
```
génère un rapport statistique (classements, etc.)

```
ds fiches machin
```
génère un PDF de résultat pour chaque élève

## Exports vers Pronote
Pronote ne proposant pas d'API, on simule des appuis clavier dans une fenêtre ouverte manuellement. Cela requiert ```xdotool``` et met la pagaille dans le presse-papier.

```
ds pronote machin classe [--option opt]
```
envoie les notes de la classe indiquée à Pronote.

```
ds bulletin classe periode [--option opt]
```
envoie à Pronote comme commentaire de bulletin le commentaire de chaque élève. Ce commentaire est la concaténation des commentaires des copies des DS de la période.


# Choix de la base de données et du répertoire de travail

Il est toujours possible de spécifier la base de données à utiliser avec ```--base /chemin/de/la/base.sqlite```.

Si cette option n'est pas présente, le script utilise un fichier ```base.sqlite``` situé dans le répertoire où se situe le script invoqué (sans déréférencer un éventuel lien symbolique vers le script).

Dans les deux cas, le répertoire de travail sera le répertoire contenant réellement le fichier de la base de données (cette fois, en déréférençant un éventuel lien symbolique).

Les rapports et les fichiers temporaires sont placés dans le répertoire temporaire du système.

# Utilisation mobile
Le script devrait fonctionner sur un téléphone Android avec l'application [Termux](https://f-droid.org/packages/com.termux/) dès lors que les dépendances sont installées.

