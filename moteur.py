class ErreurEleve(Exception):
    pass


class PlusieursEleves(ErreurEleve):
    def __init__(self, liste):
        self.liste = liste


class AucunEleve(ErreurEleve):
    pass


class ErreurNote(Exception):
    pass


class NoteExistante(ErreurNote):
    pass


class MauvaisNbItems(ErreurNote):
    pass


class ItemTropGrand(ErreurNote):
    pass


def ItemsOK(les_items, note):
    """Vérifie qu'on a bien donné des notes inférieures au barème"""
    for k in range(len(les_items)):
        if note[k] is not None and note[k] > les_items[k][1]:
            return False
    return True


def genNotes(id_eleve, question, les_items, note):
    for k in range(len(les_items)):
        if note[k] is not None:
            yield (id_eleve, question, les_items[k][0], note[k])


# Initialisation d'un DS

def Initialiser(bdd, table_etu):
    bdd.execute("""CREATE TABLE IF NOT EXISTS """ + table_etu + """ ( "id" "INTEGER" PRIMARY KEY AUTOINCREMENT,
"nom" "TEXT" NOT NULL,
"prenom" "TEXT" NOT NULL,
"genre" "TEXT" NOT NULL,
"classe" "TEXT" NOT NULL,
"option" "TEXT",
"groupe" "TEXT",
"email" "TEXT"
)""")
    bdd.execute(
        "CREATE TABLE IF NOT EXISTS classes_ds (ds TEXT NOT NULL, classe TEXT NOT NULL, option TEXT, periode TEXT NOT NULL, coeff REAL)")
    bdd.commit()
    bdd.close()


def CreerDS(bdd, nom_ds, nom_ds_etu, classes, options, periode):
    """Ajoute deux tables et une vue dans la bdd pour un DS nommé nom_ds. Une table pour la structure du DS,
    une table pour les résultats des élèves question par question, une vue pour les notes """
    # table de métadonnées
    
    if options is None or len(options) == 0:
        for classe in classes:
            bdd.execute("INSERT INTO classes_ds VALUES (?, ?, ?, ?, null);", (nom_ds, classe, None, periode))
    else:
        for classe in classes:
            for option in options:
                bdd.execute("INSERT INTO classes_ds VALUES (?, ?, ?, ?, null);", (nom_ds, classe, option, periode))
    
    # tables du DS
    bdd.execute(
        f"""CREATE TABLE IF NOT EXISTS {nom_ds}_struct (question text NOT NULL, item text NOT NULL, bareme int NOT
        NULL, points real, PRIMARY KEY(question, item))""")
    bdd.execute(f"INSERT OR IGNORE INTO {nom_ds}_struct VALUES ('penalite', '', 8, -4)")
    bdd.execute(
        """"CREATE TABLE IF NOT EXISTS {nom_ds}_result (question text NOT NULL, item text NOT NULL, id_eleve int NOT
        NULL, points int, commentaire TEXT, PRIMARY KEY(question, item, id_eleve))""")
    bdd.execute(
        f"CREATE TABLE IF NOT EXISTS {nom_ds}_copie (id_eleve int NOT_NULL, commentaire TEXT, PRIMARY KEY(id_eleve))")
    les_classes = "(" + ",".join(['"' + x + '"' for x in classes]) + ")"
    if options is None or len(options) == 0:
        critere_option = ""
    else:
        critere_option = " AND option IN (" + ",".join(['"' + x + '"' for x in options]) + ")"
    bdd.execute(
        f"""CREATE VIEW IF NOT EXISTS {nom_ds}_notes AS SELECT tbl_etu.id, tbl_etu.nom, tbl_etu.prenom,
        tbl_etu.classe, tbl_etu.groupe, tbl_etu.option, ROUND(SUM({nom_ds}_result.points * {nom_ds}_struct.points / {
        nom_ds}_struct.bareme),2) AS note, {nom_ds}_copie.commentaire AS commentaire FROM ((SELECT * FROM {
        nom_ds_etu} WHERE classe IN {les_classes + critere_option }) AS tbl_etu LEFT JOIN ({nom_ds}_result JOIN
        {nom_ds}_struct USING (question, item)) ON tbl_etu.id = {nom_ds}_result.id_eleve) LEFT JOIN {nom_ds}_copie ON
        tbl_etu.id={nom_ds}_copie.id_eleve GROUP BY tbl_etu.id""")
    bdd.commit()
    bdd.close()


class DS:
    def __init__(self, bdd, table_etu, table_ds_bn):
        self.bdd = bdd
        self.table_ds = table_ds_bn
        self.table_result = f"{table_ds_bn}_result"
        self.table_struct = f"{table_ds_bn}_struct"
        self.table_notes = f"{table_ds_bn}_notes"
        self.table_copie = f"{table_ds_bn}_copie"
        self.commit_note = True

        meta = bdd.execute("SELECT classe, option, periode FROM classes_ds WHERE ds = ?", (table_ds_bn,)).fetchall()
        self.classes = tuple(x["classe"] for x in meta)
        self.options = tuple(x["option"] for x in meta if x["option"] is not None)
        self.periode = meta[0]["periode"]

        if len(self.options) == 0:
            self.bdd.execute(
                "CREATE TEMPORARY VIEW tv_etu AS SELECT * FROM " + table_etu + " WHERE classe in (" + ",".join(
                    ["'" + x + "'" for x in self.classes]) + ")")
        else:
            self.bdd.execute(
                "CREATE TEMPORARY VIEW tv_etu AS SELECT * FROM " + table_etu + " WHERE classe in (" + ",".join(
                    ["'" + x + "'" for x in self.classes]) + ") AND option in (" + ",".join(
                    ["'" + x + "'" for x in self.options]) + ")")
        self.bdd.commit()
        self.table_etu = "tv_etu"

        self.ds = self.LireDS()
    
    def questions(self):
        return self.ds[0]
    
    def items(self):
        return self.ds[1]
    
    def select_from(self, table, champs, complement, *etc):
        return self.bdd.execute(f"SELECT {champs} FROM {table} {complement}", *etc)
    
    def select_from_struct(self, *etc):
        return self.select_from(self.table_struct, *etc)
    
    def select_from_result(self, *etc):
        return self.select_from(self.table_result, *etc)
    
    def select_from_notes(self, *etc):
        return self.select_from(self.table_notes, *etc)
    
    def select_from_etu(self, *etc):
        return self.select_from(self.table_etu, *etc)
    
    def commit(self):
        return self.bdd.commit()
    
    def maybe_commit(self):
        if self.commit_note:
            return self.bdd.commit()
    
    def close(self):
        return self.bdd.close()
    
    def LireDS(self):
        """Récupère la structure d'un DS et renvoie la liste des questions et des items, dans le même ordre"""
        lignes = self.select_from_struct("*", "ORDER BY question COLLATE ORDONNER_QUEST, rowid").fetchall()
        questions = []
        items = []
        for l in lignes:
            if questions == [] or l["question"] != questions[-1]:
                questions.append(l["question"])
                items.append([])
            items[-1].append((l["item"], int(l["bareme"])))
        return questions, items
    
    def SelectionnerEleve(self, nom, prenom):
        if prenom is None:
            liste = self.select_from_etu("*", "WHERE nom = ? COLLATE PREFIXE_NOMS", (nom,)).fetchall()
        else:
            liste = self.select_from_etu("*", "WHERE nom = ? COLLATE PREFIXE_NOMS AND prenom = ? COLLATE PREFIXE_NOMS",
                                         (nom, prenom)).fetchall()
        if len(liste) == 0:
            raise AucunEleve
        if len(liste) > 1:
            raise PlusieursEleves(liste)
        return liste[0]
    
    def SelectionnerEleveParId(self, id):
        liste = self.select_from_etu("*", "WHERE id = ?", (id,)).fetchall()
        if len(liste) == 0:
            raise AucunEleve
        if len(liste) > 1:
            raise PlusieursEleves(liste)
        return liste[0]
    
    def ItemsQuestion(self, q_nb):
        return self.ds[1][q_nb]
    
    def NomQuestion(self, q_nb):
        return self.ds[0][q_nb]
    
    def QuestionNommee(self, repq, nq_courante):
        if repq[0] == "-":
            nmoins = repq.count("-")
            if nmoins != len(repq) or nq_courante < nmoins:
                raise ValueError
            return nq_courante - nmoins
        else:
            return self.ds[0].index(repq)
    
    def NombreQuestions(self):
        return len(self.ds[0])
    
    def AncienneNote(self, eleve, q_nb):
        """Récupère la note éventuellement déjà inscrite pour cette question, en vue de son affichage"""
        note = self.select_from_result("item, points", "WHERE id_eleve = ? AND question = ? ORDER BY rowid",
                                       (eleve["id"], self.NomQuestion(q_nb))).fetchall()
        if len(note) == 0:
            return None
        elif len(note) == 1:
            return str(note[0]["points"])
        else:
            return "/".join([x["item"] + "=" + str(x["points"]) for x in note])
    
    def EffacerNote(self, id_eleve, question):
        self.bdd.execute(f"DELETE FROM {self.table_result} WHERE id_eleve = ? AND question = ?",
                         (id_eleve, question))
        self.commit()
    
    def EffacerToutesNotes(self, id_eleve):
        self.bdd.execute(f"DELETE FROM {self.table_result} WHERE id_eleve = ?", (id_eleve,))
        self.commit()
    
    def EnregistrerNote(self, id_eleve, q_nb, note, replace):
        """Effectue l'inscription d'une note dans la BDD. En cas de conflit (note déjà existante), lève une exception
        si replace est False. Renvoie le fait qu'on a réussi l'inscription et le fait qu'un remplacement a été
        effectué. """
        les_items = self.ItemsQuestion(q_nb)
        question = self.NomQuestion(q_nb)
        if len(note) != len(les_items):
            raise MauvaisNbItems
        if not ItemsOK(les_items, note):
            raise ItemTropGrand
        if replace:
            self.EffacerNote(id_eleve, question)
        try:
            self.bdd.executemany(
                f"INSERT OR ROLLBACK INTO {self.table_result} (id_eleve, question, item, points) VALUES (?, ?, ?, ?)",
                genNotes(id_eleve, question, les_items, note))
            self.maybe_commit()
            return True, replace
        except self.bdd.IntegrityError:
            raise NoteExistante
    
    def NoteTotale(self, id_eleve):
        return self.select_from_notes("note", "WHERE id = ?", (id_eleve,)).fetchone()["note"]
    
    def EnregistrerCommentaire(self, id_eleve, texte):
        if texte == "":
            self.EffacerCommentaire(id_eleve)
        else:
            self.bdd.execute(f"INSERT OR REPLACE INTO {self.table_copie} VALUES (?, ?)", (id_eleve, texte))
            self.maybe_commit()
    
    def AugmenterCommentaire(self, id_eleve, texte):
        anc_comm = self.AncienCommentaire(id_eleve)
        if anc_comm is None or anc_comm == "":
            self.EnregistrerCommentaire(id_eleve, texte)
        else:
            self.EnregistrerCommentaire(id_eleve, anc_comm + " " + texte)
    
    def AncienCommentaire(self, id_eleve):
        res = self.bdd.execute(f"SELECT commentaire FROM {self.table_copie} WHERE id_eleve = ?",
                               (id_eleve,)).fetchall()
        if len(res) == 0:
            return None
        else:
            return res[0]["commentaire"]
    
    def EffacerCommentaire(self, id_eleve):
        self.bdd.execute(f"DELETE FROM {self.table_copie} WHERE id_eleve = ?", (id_eleve,))
        self.maybe_commit()
    
    def ParseCommentaire(self, id_eleve, texte):
        if texte == "" or texte == "*":
            return
        if texte == "****":
            self.EffacerCommentaire(id_eleve)
        else:
            if texte[-1] not in ".?!":
                texte += "."
            if texte[0] == "*":
                self.EnregistrerCommentaire(id_eleve, texte[1:])
            else:
                self.AugmenterCommentaire(id_eleve, texte)
