from interf_texte import *
from moteur import PlusieursEleves, AucunEleve, MauvaisNbItems, ItemTropGrand


class InterfaceBatch(InterfaceAuto):
    def __init__(self, fich_base, table_etu, table_ds_bn, nom_fichier, action_doublon):
        super().__init__(fich_base, table_etu, table_ds_bn, nom_fichier, action_doublon)
    
    def LigneSuivante(self):
        ligne = self.f.readline()
        if ligne == "":
            raise Sortie
        return ligne.strip()
    
    def AbandonnerCopie(self):
        while self.LigneSuivante() != "---":
            pass
    
    def DesambiguerEleve(self, l_eleves, interactif):
        """Permet de choisir un élève parmi une liste."""
        reponse = -1
        print("Plusieurs choix possibles")
        for k in range(len(l_eleves)):
            print(k + 1, " : ", list(l_eleves[k]))
        while reponse not in range(len(l_eleves) + 1):
            try:
                reponse = int(input("Quel élève (0 pour abandonner) ? "))
            except:
                reponse = -1
        if reponse == 0:
            if interactif:
                return None
            else:
                raise AbandonDebut
        return l_eleves[reponse - 1]
    
    def InputEleve(self, bad_name):
        """Demande le nom d'un élève en vue de sa sélection."""
        eleve = None
        while eleve is None:
            nom = input("Entrez le nom correct de l'élève \"" + bad_name + "\" (---- pour abandonner) ")
            if nom == "----":
                raise AbandonDebut
            if " " in nom:
                (nom, prenom) = nom.split(" ")
            else:
                prenom = None
            try:
                eleve = self.ds.SelectionnerEleve(nom, prenom)
            except AucunEleve:
                print("Aucun résultat")
            except PlusieursEleves as e:
                eleve = self.DesambiguerEleve(e.liste, True)
        return eleve
    
    def LireEleve(self):
        """Demande le nom d'un élève en vue de sa sélection."""
        nom_lu = self.LigneSuivante()
        if nom_lu == "----":
            raise Sortie
        if " " in nom_lu:
            (nom, prenom) = nom_lu.split(" ")
        else:
            nom, prenom = nom_lu, None
        try:
            return self.ds.SelectionnerEleve(nom, prenom)
        except AucunEleve:
            print("Aucun résultat")
            return self.InputEleve(nom_lu)
        except PlusieursEleves as e:
            return self.DesambiguerEleve(e.liste, False)
    
    def InputNote(self, eleve, q_nb):
        return self.ParseNote(self.LigneSuivante())
    
    def TraiterQuestion(self, eleve, q_nb):
        """Gère le processus de saisie d'une note pour une question, c'est-à-dire qu'on signale les saisies incorrectes détectées par les fonctions auxiliaires, qu'on vérifie que la question demandée existen qu'on a rentré le bon nombre d'items, que les notes sont possibles et tente l'inscription effective de cette note.
        Renvoie le fait qu'on a réussi ce qu'on voulait, ce qui permet à la fonction appelante de savoir si elle doit passer à la question suivante ou rester sur celle-ci."""
        try:
            (replace, repq, note) = self.InputNote(eleve, q_nb)
        except ValueError:
            print("Erreur de saisie")
            raise AbandonMilieu(eleve["id"])
        except SaisieCommentaire as e:
            self.ds.ParseCommentaire(eleve["id"], e.texte)
            return q_nb, False
        if repq is None:
            question = self.ds.NomQuestion(q_nb)
        else:
            try:
                q_nb = self.ds.QuestionNommee(repq, q_nb)
                question = repq
            except ValueError:
                print("Question " + repq + " introuvable")
                raise AbandonMilieu(eleve["id"])
        if note is None:
            return q_nb, repq is None
        if note == ActionNote.Delete:
            try:
                self.ds.EffacerNote(eleve["id"], question)
                return q_nb, True
            except:
                print("Impossible d'effacer la note pour la question", question)
                raise AbandonMilieu(eleve["id"])
        try:
            return q_nb, self.InsereNote(eleve["id"], q_nb, note, replace)
        except MauvaisNbItems:
            print("Mauvais nombre d'items pour la question " + question)
            raise AbandonMilieu(eleve["id"])
        except ItemTropGrand:
            print("Valeur trop élevée (" + str(note) + ") pour un item en question " + question)
            raise AbandonMilieu(eleve["id"])
    
    def TraiterCopie(self):
        """Traite une copie en demandant d'abord à qui elle est, puis en itérant dans les questions du sujet"""
        try:
            eleve = self.LireEleve()
        except ValueError:
            print("Erreur de saisie")
            raise AbandonDebut
        print("Copie de ", list(eleve))
        if len(self.ds.select_from_result("*", "WHERE id_eleve = ?", (eleve["id"],)).fetchall()) > 0:
            action = self.QueFaireDoublon()
            if action is ActionDoublon.Ignorer:
                raise AbandonDebut
            if action is ActionDoublon.Ecraser:
                self.ds.EffacerToutesNotes(eleve["id"])
                self.ds.EffacerCommentaire(eleve["id"])
        q_nb = 0
        try:
            while True:
                (q_traitee, reussi) = self.TraiterQuestion(eleve, q_nb)
                if reussi:
                    q_nb = q_traitee + 1
                else:
                    q_nb = q_traitee
        except FinCopie:
            self.ds.commit()
            self.NoteTotale(eleve)
            return
