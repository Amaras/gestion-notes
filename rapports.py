import datetime
import math
import subprocess
import tempfile
from contextlib import redirect_stdout

from statistiques import enrichir_rangs, histogramme


def codeheure():
    return datetime.datetime.now().strftime("%Y%m%d%H%M%S")


# création d'un fichier temporaire avec nom aléatoire (pour le .tex).
def MakeTempFile(pref, suff):
    return tempfile.mkstemp(suffix=suff, prefix=pref, text=True)


# Production de rapports, fiches, etc.

# Technique LaTeX

def escape_latex(s):
    return s.replace("_", r"\_")


class LaTeX:
    preambule_def = r"""\pagestyle{empty}
    \usepackage[lighttt]{lmodern}
    \usepackage{alltt}
    \usepackage[nott]{kpfonts}"""
    
    # Pour qu'on puisse faire print(..., file=ltx)
    # def write(self, s):
    #     self.__f.write(s)
    # Inutile par l'utilisation de redirect_stdout
    
    def __init__(self, basename, nf_preambule, twoside=False):
        """Crée un nouveau .tex temporaire et écrit son préambule"""
        try:
            with open(nf_preambule) as fp:
                preambule = ["%%% Preambule inclus depuis " + nf_preambule + "\n"] + fp.readlines()
        except:
            preambule = ["%%% Preambule par defaut\n"] + LaTeX.preambule_def.splitlines()
        (fd, self.__fn) = MakeTempFile(basename, ".tex")
        self.__f = open(fd, "w")
        with redirect_stdout(self.__f):
            print(r"""\documentclass[a4paper,10pt, french, DIV=16,""", "twoside" if twoside else "", r"""]{scrartcl}
            \usepackage[french]{babel}
            \usepackage{t1enc}
            \usepackage{longtable}
            \setlength\LTpre{0pt}
            \setlength\LTpost{.5ex}
            \usepackage{listings}
            \usepackage{csquotes}
            \lstnewenvironment{lstsql}{\lstset{language=sql, breaklines=true, breakatwhitespace=true, breakautoindent=false, breakindent=0pt, basicstyle=\ttfamily}}{}
            \usepackage{pgfplots}""")
    
            for l in preambule:
                print(l, end="")
    
            print(r"\begin{document}")
    
    def finaliser(self, compiler):
        """Termine le .tex, le ferme, et éventuellement le compile et affiche le pdf"""
        with redirect_stdout(self.__f):
            print(r"""\end{document}""")
        self.__f.close()
        if compiler:
            self.__runLaTeX()
            self.__showPDF()
    
    def __runLaTeX(self):
        subprocess.call(
            ["latexmk", "-xelatex", "-bibtex-cond", "-cd", "-interaction=batchmode", "-halt-on-error", self.__fn])
        subprocess.call(["latexmk", "-cd", "-c", self.__fn])
    
    def __showPDF(self):
        try:
            subprocess.Popen(["xdg-open", self.__fn.replace(".tex", ".pdf")], stdin=None, stdout=None, stderr=None)
        except:
            print("L'ouverture du fichier PDF a échoué, ouvrez-le manuellement depuis",
                  self.__fn.replace(".tex", ".pdf"))
    
    def write_sql(self, bdd, req, param=None, reprNone="---", entete=False, align="l"):
        """Écrit dans le .tex la réponse à une requête sql SELECT"""
        with redirect_stdout(self.__f):  # Bien plus facile à utiliser que print(*, file=f) à chaque ligne
            try:
                if param is None:
                    cur = bdd.execute(req)
                else:
                    cur = bdd.execute(req, param)
                cles = bdd.getKeys(cur)
                resultat = cur.fetchall()
            except Exception as e:
                print("Erreur", str(e))
                return
            if entete:
                print(r"\noindent Résultat de la requête SQL~:")
                print(r"\begin{lstsql}")
                print(req)
                print(r"\end{lstsql}")
                print(r"Nombre de lignes~:", len(resultat), r"\\")
            self.write_table(cles, resultat, reprNone, align)
    
    def write_table(self, cles, donnees, reprNone="---", align="l"):
        """Écrit dans le .tex des données tabulaires qui sont une liste de sqlite.Row ou une liste de dictionnaires.
        cles est la liste des clés (champs), doit être dans le même ordre que les "colonnes" des donnees """
        with redirect_stdout(self.__f):
            print(r"\noindent")
            print(r"\begin{longtable}[" + align + r"]{|" + "l|" * len(cles) + "}")
            print(r"\hline")
            try:
                print("&".join([r"\textbf{" + escape_latex(str(h)) + "}" for h in cles]) + r"\\\hline\endfirsthead")
                print(r"\hline")
                print(r"\multicolumn{", len(cles), r"}{|c|}{Suite de la page précédente}\\\hline")
                print("&".join([r"\textbf{" + escape_latex(str(h)) + "}" for h in cles]) + r"\\\hline\endhead")
                print(r"\multicolumn{", len(cles), r"}{|c|}{Suite sur la page suivante}\\\hline\endfoot")
                print(r"\endlastfoot")
                for ligne in donnees:
                    print("&".join(
                        [reprNone if ligne[k] is None else escape_latex(str(ligne[k])) for k in cles]) + r"\\\hline")
            except Exception as e:
                print("Erreur", escape_latex(str(e)))
            print(r"\end{longtable}")
    
    def write_histo(self, classes, l_donnees, titres=None, align="l", hauteur=5):
        largeur = str(20 / len(l_donnees))
        with redirect_stdout(self.__f):
            print(r"\leavevmode")
            if align != "l":
                print(r"\hfill")
            print(r"""\begin{tikzpicture}
            \begin{axis}[width=18cm, height=""" + str(
                hauteur) + r"""cm, ybar , fill, hide y axis, axis x line*=bottom, x axis line style={draw opacity=0}, axis line shift=""" + str(
                -(14 + (
                    hauteur - 5) * 1.7)) + r"""pt, xtick=data, tick style={draw opacity=0 }, bar width=""" + largeur + r"""pt,nodes near coords, symbolic x coords = {""")
            print(",".join(classes))
            
            print(r"""},x tick label style={font=\scriptsize}, node near coord style={font=\scriptsize}]""")
            for kd in range(len(l_donnees)):
                donnees = l_donnees[kd]
                print(r"""\addplot coordinates
                {""")
                for k in range(len(classes)):
                    print("(" + str(classes[k]) + ",", donnees[k], ")")
                print(r"""};""")
                if titres is not None:
                    print(r"""\addlegendentry{""", titres[kd], r"""}""")
            print(r"""
            \end{axis}
            \end{tikzpicture}""")
            if align == "c":
                print(r"\hfill\null")


# Rapport

def titre(s, ltx):
    print(r"\begin{center}\Large ", s, r"\end{center}", file=ltx)


def recup_donnees(bdd, table_ds):
    les_classes = [x[0] for x in
                   bdd.execute("select distinct classe from " + table_ds + "_notes ORDER BY classe ASC").fetchall()]
    donnees = [dict(x) for x in bdd.execute(
        "select classe, groupe, option, nom, prenom, note, STRROUND(note, 2) AS dnote, commentaire from " + table_ds + "_notes order by note desc, nom collate ordonner_noms, prenom collate ordonner_noms;").fetchall()]
    enrichir_rangs(donnees, "note", None, "rang")
    enrichir_rangs(donnees, "note", "classe", "rang_classe")
    cles = ["classe", "groupe", "option", "nom", "prenom", "dnote", "rang", "rang_classe"]
    # histogramme
    cases = ["{[" + str(2 * n) + "," + str(2 * (n + 1)) + "[}" for n in range(10)]
    cases.append(r"{\strut20+}")
    histo_gen = histogramme(donnees, "note", None, lambda n: int(math.floor(n)) // 2 if n < 20 else 10, 11)
    histo_classes = histogramme(donnees, "note", "classe", lambda n: int(math.floor(n)) // 2 if n < 20 else 10, 11)
    return les_classes, donnees, cles, cases, histo_gen, histo_classes


def Rapport(table_ds, table_etu, bdd, nf_preambule):
    """Génère un rapport avec des stats, des classements, etc."""
    ltx = LaTeX(table_ds + "_rapport_" + codeheure() + "_", nf_preambule)
    les_classes, donnees, cles, cases, histo_gen, histo_classes = recup_donnees(bdd, table_ds)
    # stats générale,s à suivre
    titre("Statistiques", ltx)
    with redirect_stdout(ltx):
        for req in [
                            """select classe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + "_notes group by classe;",
                            """select classe, groupe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + "_notes group by classe, groupe;"]:
            ltx.write_sql(bdd, req, align="r")
            print(r"\bigskip")
    
        # ltx.write_table(range(21), [histo_gen])
        if len(les_classes) > 1:
            ltx.write_histo(cases, [histo_gen] + [histo_classes[x] for x in sorted(histo_classes.keys())],
                            titres=["Global"] + sorted(histo_classes.keys()), align="r")
            print(r"\bigskip")
        
            ltx.write_sql(bdd,
                          """select question, item,  points AS "barème",Répondu, CAST(100*Min/bareme AS int)||" \%" AS "Réussite mini", CAST(100*Max/bareme AS int)||" \%" AS "Maxi", CAST(100*Moy/bareme AS int)||" \%" AS "Moy", CAST(100*ect/bareme AS int)||" pt\%" AS "Écart-type" from (SELECT question, item, bareme, points FROM """ + table_ds + """_struct) NATURAL LEFT JOIN (select question, item, COUNT(*) AS "Répondu", STRROUND(MIN(points),2) AS "Min", STRROUND(MAX(points),2) AS "Max", STRROUND(AVG(points),2) AS "Moy", STRROUND(STDEV(points),2) AS "ect" from """ + table_ds + "_result GROUP BY question, item) ORDER BY question COLLATE ORDONNER_QUEST;",
                          align="r")
    
        print(r"\clearpage")
        # notes
        if len(les_classes) > 1:
            titre("Classement général", ltx)
            ltx.write_table(cles, donnees, align="c")
            print(r"\clearpage")
        for classe in les_classes:
            donnees_classe = [eleve for eleve in donnees if eleve["classe"] == classe]
            titre(classe + " --- Classement", ltx)
            print(r"\bigskip")
            ltx.write_table(cles, donnees_classe, align="c")
            print(r"\clearpage")
            titre(classe + " --- Notes", ltx)
            donnees_classe.sort(key=lambda x: x["nom"])
            ltx.write_table(cles, donnees_classe, align="c")
            print(r"\clearpage")
            titre(classe + " --- Statistiques", ltx)
            ltx.write_histo(cases, [histo_classes[classe]], align="c", hauteur=3.5)
            ltx.write_sql(bdd,
                          """select question, item, points AS "barème", Répondu, CAST(100*Min/bareme AS int)||" \%" AS "Réussite mini", CAST(100*Max/bareme AS int)||" \%" AS "Maxi", CAST(100*Moy/bareme AS int)||" \%" AS "Moy", CAST(100*ect/bareme AS int)||" pt\%" AS "Écart-type" from (SELECT question, item, bareme, points FROM """ + table_ds + """_struct) NATURAL LEFT JOIN (select question, item, COUNT(*) AS "Répondu", STRROUND(MIN(points),2) AS "Min", STRROUND(MAX(points),2) AS "Max", STRROUND(AVG(points),2) AS "Moy", STRROUND(STDEV(points),2) AS "ect" from (SELECT * FROM """ + table_ds + "_result JOIN " + table_etu + " ON id_eleve=id WHERE classe=?) GROUP BY question, item) ORDER BY question COLLATE ORDONNER_QUEST;",
                          param=(classe,), align="c")
        
            print(r"\clearpage")
            titre(classe + " --- Commentaires", ltx)
            for x in donnees_classe:
                if x["commentaire"] is not None:
                    print(r"""\paragraph{""", x["nom"], r"""(""" + str(x["dnote"]) + r""")} """, x["commentaire"],
                          r"""\par""")
            print(r"\clearpage")
    bdd.close()
    ltx.finaliser(True)


# Fiches individuelles



def Fiches(table_ds, table_etu, bdd, nf_preambule, classe=None, groupe=None, stats_par_groupes=False):
    """Génère une fiche pour chaque élève. On peut restreindre à une liste de classes et/ou de groupes. On peut choisir d'afficher des statistiques par groupe"""
    ltx = LaTeX(table_ds + "_fiches_" + codeheure() + "_", nf_preambule, True)
    les_classes, donnees, cles, cases, histo_gen, histo_classes = recup_donnees(bdd, table_ds)
    with redirect_stdout(ltx):
        print(r"""\newbox\boxstat
        \def\stats{\usebox\boxstat}
        \setbox\boxstat=\vbox{\small%""")
        if len(les_classes) > 1:
            ltx.write_sql(bdd,
                          """select count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + """_notes;""",
                          align="r")
        ltx.write_sql(bdd,
                      """select classe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + """_notes group by classe;""",
                      align="r")
        if stats_par_groupes:
            ltx.write_sql(bdd,
                          """select classe, groupe, count(note) as "Copies", STRROUND(min(note),2) as "Note min", STRROUND(max(note),2) as "Note max", STRROUND(avg(note),2) as "Moyenne", STRROUND(stdev(note),2) as "Écart-type" from """ + table_ds + """_notes group by classe, groupe;""",
                          align="r")
        if len(les_classes) == 1:
            ltx.write_histo(cases, [histo_gen], align="r")
        else:
            ltx.write_histo(cases, [histo_gen] + [histo_classes[x] for x in sorted(histo_classes.keys())],
                            titres=["Global"] + sorted(histo_classes.keys()), align="r")
        print(r"""}""")
        eleves_notes = [dict(x) for x in bdd.execute(
            "SELECT *, STRROUND(note, 2) AS dnote FROM " + table_ds + "_notes order by classe, nom collate ordonner_noms").fetchall()]
        enrichir_rangs(eleves_notes, "note", None, "rang")
        enrichir_rangs(eleves_notes, "note", "classe", "rang_classe")
        for eleve in eleves_notes:
            if (classe is None or eleve["classe"] == classe) and (groupe is None or eleve["groupe"] == groupe):
                fiche_eleve(ltx, bdd, table_ds, eleve)
                print(r"""\cleardoublepage""")
    bdd.close()
    ltx.finaliser(True)


def fiche_eleve(ltx, bdd, table_ds, eleve):
    with redirect_stdout(ltx):
        print(r"\begin{center}\LARGE " + eleve["nom"] + " " + eleve["prenom"] + r"\\")
        print(r"\large " + eleve["classe"] + r"\end{center}")
        print(r"\begingroup")
        print("Note :", str(eleve["dnote"]), r"/ 20\hfill")
        print("Rang dans la classe :", eleve["rang_classe"], r"\hfill")
        print("Rang global :", eleve["rang"])
        print(r"\endgroup")
        print(r"\stats")
        if eleve["commentaire"] is not None and eleve["commentaire"] != "":
            print(r"\section*{Commentaire}\begingroup\small", eleve["commentaire"], r"\endgroup", )
        print(r"\section*{Détail de votre copie}\begingroup\small")
        ltx.write_sql(bdd,
                      """select s.question, s.item, CAST(STRROUND(100*r.points/s.bareme, 0) as int)||" \%" as "Réussite", s.points as "Barème", STRROUND(r.points*s.points/s.bareme,2) as "Vos points" from """ + table_ds + """_struct as s left join (select * from """ + table_ds + """_result where id_eleve=?) as r on s.question=r.question and s.item=r.item order by s.question collate ordonner_quest, s.rowid;""",
                      (eleve["id"],))
        print(r"\endgroup")
